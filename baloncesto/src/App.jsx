import React, { useState } from 'react';
import Header from './components/Header';
import PlayerTable from './components/PlayerTable';
import SearchBar from './components/SearchBar';
import Buttons from './components/Buttons';
import './App.css';

const App = () => {
  const [players, setPlayers] = useState([
    { number: 3, name: 'Kevin Harrell', team: 'Top Club', position: 'Guard' },
    { number: 5, name: 'James Fletcher', team: 'Top Club', position: 'Center' },
    { number: 14, name: 'Jeff Montes', team: 'Top Club', position: 'Forward-Guard' },
    { number: 27, name: 'Bryan Warner', team: 'Top Club', position: 'Forward-Center' },
    { number: 30, name: 'Scott Dale', team: 'Top Club', position: 'Forward' },
    { number: 37, name: 'Noah Jones', team: 'Top Club', position: 'Guard' },
  ]);
  const [filteredPlayers, setFilteredPlayers] = useState(players);

  const handleSearch = (query) => {
    const result = players.filter(player => player.name.toLowerCase().includes(query.toLowerCase()));
    setFilteredPlayers(result);
  };

  const handleEven = () => {
    document.querySelectorAll('.odd').forEach(row => row.style.backgroundColor = 'white');
    document.querySelectorAll('.even').forEach(row => row.style.backgroundColor = 'lightgray');
  };

  const handleOdd = () => {
    document.querySelectorAll('.even').forEach(row => row.style.backgroundColor = 'white');
    document.querySelectorAll('.odd').forEach(row => row.style.backgroundColor = 'lightgray');
  };

  const handleRange1To10 = () => {
    const result = players.filter(player => player.number >= 1 && player.number <= 10);
    setFilteredPlayers(result);
  };

  const handleRange11To20 = () => {
    const result = players.filter(player => player.number >= 11 && player.number <= 20);
    setFilteredPlayers(result);
  };

  return (
    <div className="App">
      <Header />
      <div className="content">
        <h1 className="content__title">ROSTER</h1>
        <SearchBar onSearch={handleSearch} />
        <Buttons 
          onEven={handleEven} 
          onOdd={handleOdd} 
          onRange1To10={handleRange1To10} 
          onRange11To20={handleRange11To20} 
        />
        <PlayerTable players={filteredPlayers} />
      </div>
    </div>
  );
};

export default App;
