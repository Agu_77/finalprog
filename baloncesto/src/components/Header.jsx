import React from 'react';
import '../App.css';

const Header = () => (
  <header className="App__header">
    <img src="logo.png" alt="Top Club Logo" className="App__logo" />
    <nav className="App__nav">
      <ul className="App__nav-list">
        <li className="App__nav-item">Home</li>
        <li className="App__nav-item">Pages</li>
        <li className="App__nav-item">SportsPress</li>
        <li className="App__nav-item">Shop</li>
        <li className="App__nav-item">Purchase</li>
      </ul>
    </nav>
  </header>
);

export default Header;
