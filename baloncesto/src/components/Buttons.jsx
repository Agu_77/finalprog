import React from 'react';
import '../App.css';

const Buttons = ({ onEven, onOdd, onRange1To10, onRange11To20 }) => (
  <div className="buttons">
    <button className="buttons__button" onClick={onEven}>Pintar Pares</button>
    <button className="buttons__button" onClick={onOdd}>Pintar Impares</button>
    <button className="buttons__button" onClick={onRange1To10}>Traer de 1 a 10</button>
    <button className="buttons__button" onClick={onRange11To20}>Traer de 11 a 20</button>
  </div>
);

export default Buttons;

