import React from 'react';
import '../App.css';

const PlayerTable = ({ players }) => {
  return (
    <div className="player-table">
      <table className="player-table__table">
        <thead className="player-table__thead">
          <tr className="player-table__header-row">
            <th className="player-table__header-cell">#</th>
            <th className="player-table__header-cell">Player</th>
            <th className="player-table__header-cell">Team</th>
            <th className="player-table__header-cell">Position</th>
          </tr>
        </thead>
        <tbody className="player-table__tbody">
          {players.map((player, index) => (
            <tr key={player.number} className={`player-table__row ${index % 2 === 0 ? 'even' : 'odd'}`}>
              <td className="player-table__cell">{player.number}</td>
              <td className="player-table__cell">{player.name}</td>
              <td className="player-table__cell">{player.team}</td>
              <td className="player-table__cell">{player.position}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default PlayerTable;
