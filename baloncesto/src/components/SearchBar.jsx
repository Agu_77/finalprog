import React from 'react';

const SearchBar = ({ onSearch }) => (
  <input 
    type="text" 
    placeholder="Buscar por nombre jugador..." 
    onChange={(e) => onSearch(e.target.value)} 
    className="search-bar"
  />
);

export default SearchBar;
